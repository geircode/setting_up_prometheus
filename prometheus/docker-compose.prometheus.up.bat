REM Run Dockerfile.build.bat first if you want to create your own Container Image.
cd %~dp0
docker rm -f prometheus-1
docker-compose -f docker-compose.prometheus.yml down --remove-orphans
docker-compose -f docker-compose.prometheus.yml up --build --remove-orphans
pause
docker exec -it prometheus-1 /bin/sh