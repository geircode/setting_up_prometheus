# This Container will monitor a list of script containers and report how they run

Usage:

- Run *Dockerfile.build.bat* first if you want to create your own Container Image.
- Run *docker-compose.up.bat* to start the workspace container

Prometheus:

- Navigate to the "prometheus" folder
- Run *docker-compose.prometheus.up.bat* to start the server
