import unittest
import datetime
from prometheus.DockerService import DockerService

class TestDockerService(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.dockerService = DockerService()
        
    def setUp(self):
        print(datetime.datetime.now())   

    def test_list_running_containers(self):
        containers = self.dockerService.getContainers()
        for item in containers:
            print(item)


if __name__ == '__main__':
    # environmentService = EnvironmentService()
    # environmentService.setPythonEnv(EnvironmentSetting.local)       
    # environmentService.setLocalDockerTargetEnv(EnvironmentSetting.local)        
    unittest.main()
